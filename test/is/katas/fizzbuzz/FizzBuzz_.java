package is.katas.fizzbuzz;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class FizzBuzz_ {

    @Test
    public void of_0_is_0(){
        assertThat(FizzBuzz.of(0), is("0"));
    }

    @Test
    public void of_1_is_1(){
        assertThat(FizzBuzz.of(1), is("1"));
    }

    @Test
    public void of_3_is_fizz(){
        assertThat(FizzBuzz.of(3), is("Fizz"));
    }

    @Test
    public void of_5_is_buzz(){
        assertThat(FizzBuzz.of(5), is("Buzz"));
    }


    @Test
    public void of_6_is_fizz(){
        assertThat(FizzBuzz.of(6), is("Fizz"));
    }

    @Test
    public void of_10_is_buzz(){
        assertThat(FizzBuzz.of(10), is("Buzz"));
    }

    @Test
    public void of_15_is_fizzbuzz(){
        assertThat(FizzBuzz.of(15), is("FizzBuzz"));
    }

    @Test
    public void acceptance(){
        assertThat(FizzBuzz.of(45), is("FizzBuzz"));
        assertThat(FizzBuzz.of(35), is("Buzz"));
        assertThat(FizzBuzz.of(34), is("34"));
        assertThat(FizzBuzz.of(33), is("Fizz"));
        assertThat(FizzBuzz.of(102), is("Fizz"));
        assertThat(FizzBuzz.of(60), is("FizzBuzz"));
    }
}
