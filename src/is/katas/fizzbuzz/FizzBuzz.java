package is.katas.fizzbuzz;

import static java.lang.String.*;

public class FizzBuzz {

    public static String of(int number) {
        if (is(number).divisibleBy(15)) return "FizzBuzz";
        if (is(number).divisibleBy(3)) return "Fizz";
        if (is(number).divisibleBy(5)) return "Buzz";
        return valueOf(number);
    }

    private static FizzBuzz.Checker is (int number){
        return divisor -> number != 0 && number % divisor == 0;
    }

    private interface Checker {
        default boolean isNotZero(int number) {
            return !(number == 0);
        }
        boolean divisibleBy(int divisor);
    }
}
